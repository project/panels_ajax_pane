<?php
/**
 * A simple encryption and decryption scheme.We really should be using AEAD, 
 * but since there isn't good support for this block-mode yet, we use
 * Encrypt-Then-HMAC.
 * 
 * See https://cseweb.ucsd.edu/~mihir/papers/oem.pdf
 */

$panels_ajax_pane_crypto_cipher = "aes-256-gcm";
$panels_ajax_pane_crypto_test_mode = false;

/**
 * Encrypt data using 256-bit AES-CBC Encrypt-Then-Mac
 * 
 * @param string $data 
 *   Data to be encrypted. Only string data is allowed. To encrypt non-string
 *   data it should be json-encoded or serialized first. 
 * 
 * @return string
 *   JSON encoded result of the encryption. Can be passed
 *   to panels_ajax_pane_decrypt_data to decrypt
 */
function panels_ajax_pane_encrypt_data($data) {
  global $panels_ajax_pane_crypto_cipher;
  if (!is_string($data)) {
    throw new Exception('panels_ajax_pane: Can only encrypt string data.');
  }

  $cipher = $panels_ajax_pane_crypto_cipher;
  if (!in_array($cipher, openssl_get_cipher_methods())) {
    throw new Exception('panels_ajax_pane: openssl is missing ' . $cipher . ' cipher');
  }
  
  $params = panels_ajax_pane_crypto_params();

  $key = hash_hkdf('sha256', $params['aes_key'], 32);
  $ivlen = openssl_cipher_iv_length($cipher);
  $iv = openssl_random_pseudo_bytes($ivlen);

  $tag = ''; // openssl_encrypt() will fill this in.
  $encrypted = openssl_encrypt($data, $cipher, $key, $options=0, $iv, $tag); 

  // Check to make sure it encrypted corrected
  if (empty($encrypted)) {
    throw new Exception('panels_ajax_pane: Failed to encrypt crypto data.');
  }

  // Pack it and ship it
  $packed = json_encode(array('encrypted' => base64_encode($encrypted), 'iv' => base64_encode($iv), 'tag' => base64_encode($tag)));

  return $packed;
}

/**
 * Decrypt data that was encrypted using panels_ajax_pane_encrypt_data()
 * 
 * See panels_ajax_pane_encrypt_data() for a full description
 * 
 * @param string $encrypted_json
 */
function panels_ajax_pane_decrypt_data($encrypted_json) {
  global $panels_ajax_pane_crypto_cipher;
  if (empty($encrypted_json)) {
    return FALSE;
  }

  $encrypted = json_decode($encrypted_json, true);

  $cipher = $panels_ajax_pane_crypto_cipher;
  if (!in_array($cipher, openssl_get_cipher_methods())) {
    throw new Exception('panels_ajax_pane: openssl is missing ' . $cipher . ' cipher');
  }

  $params = panels_ajax_pane_crypto_params();

  $key = hash_hkdf('sha256', $params['aes_key'], 32);
  $iv = base64_decode($encrypted['iv']);
  $tag = base64_decode($encrypted['tag']);
  $encrypted = base64_decode($encrypted['encrypted']);

  $original_plaintext = openssl_decrypt($encrypted, $cipher, $key, $options=0, $iv, $tag);

  return $original_plaintext;
}

/**
 * Get / generate cypto parameters
 * 
 * This will automatially generate strong crypto paramaters and store them in cache
 * If crypto params already exist in cache, it will use them. 
 */
function panels_ajax_pane_crypto_params() {
  // If we are in test-mode, skip the drupal stuff
  global $panels_ajax_pane_crypto_test_mode;
  if ($panels_ajax_pane_crypto_test_mode) {
    return array(
      'aes_key' => '0000000000000',
    );
  }

  if ($params = variable_get('panels_ajax_pane_crypto_params')) {
    return $params;
  }

  $params = array(
    'aes_key' => bin2hex(openssl_random_pseudo_bytes(16)),
  );

  variable_set('panels_ajax_pane_crypto_params', $params);

  return $params;
}

// To run tests: 
// php -r "require 'panels_ajax_pane.crypto.inc'; panels_ajax_pane_crypto_test();"
function panels_ajax_pane_crypto_test() {
  global $panels_ajax_pane_crypto_test_mode;
  $panels_ajax_pane_crypto_test_mode = true;

  $encrypted = panels_ajax_pane_encrypt_data("test");
  $decrypted = panels_ajax_pane_decrypt_data($encrypted);

  if ($decrypted != "test") {
    throw new Exception("test failed");
  }
}