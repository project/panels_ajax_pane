<?php

require_once('panels_ajax_pane.crypto.inc');

/**
 * Implements hook_ctools_plugin_directory().
 */
function panels_ajax_pane_ctools_plugin_directory($module, $plugin) {
  if (!empty($plugin)) {
    return "plugins/$plugin";
  }
}

/**
 * Implements hook_panels_pre_render().
 */
function panels_ajax_pane_panels_pre_render($display, $renderer) {
  if (!empty($display->content)) {

    // If it's already an AJAX rendered page, don't bother putting ajax in ajax
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH'])) {
      if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        return;
      }
    }

    foreach ($display->content as $key => $pane) {
      $plugin = ctools_get_plugins('ctools', 'content_types', $pane->subtype);
      if (!empty($plugin['panels_ajax_pane'])) {
        $config = array();

        $pid = is_numeric($pane->pid) ? 'auto-' . $pane->pid : $pane->pid;

        $config['pid'] = $pid;
        $config['args'] = $display->args;
        $config['type'] = $pane->type;
        $config['subtype'] = $pane->subtype;
        $config['conf'] = $pane->configuration;
        $config['keywords'] = array(); //TODO: Where does this come from?
        $config['_GET'] = $_GET;
        $config['context'] = array();
        foreach ($display->context as $context_id => $context) {
          $config['context'][$context_id] = array(
            'name' => $context->plugin,
            'context_settings' => !empty($context->context_settings) ? $context->context_settings : array(), 
            'keyword' => $context->keyword,
            'identifier' => $context->identifier, 
            'argument' => $context->argument, 
          );
        }

        // Add type & subtype classes from original plugin.
        $classes = array();
        ctools_include('cleanstring');
        $type_class = $pane->type ? 'pane-'. ctools_cleanstring($pane->type, array('lower case' => TRUE)) : '';
        $subtype_class = $pane->subtype ? 'pane-'. ctools_cleanstring($pane->subtype, array('lower case' => TRUE)) : '';
        $classes[] = $type_class;
        if ($type_class != $subtype_class) {
          $classes[] = $subtype_class;
        }

        // Determine Title, will either be dyanmic (replaced on ajax) or static
        if ($pane->configuration['override_title']) {
          $title_text = $pane->configuration['override_title_text'];
        }
        else {
          $title_text = "<span class='panels-ajax-pane-title' data-pid='$pid'></span>";
        }

        $display->content[$key]->subtype = 'panels_ajax_pane_content';
        $display->content[$key]->type = 'panels_ajax_pane_content';
        $display->content[$key]->configuration = array(
          'pid' => $pid,
          'add_class' => $classes,
          'override_title' => 1,
          'override_title_text' => $title_text,
        );
        if (!empty($pane->configuration['override_title_heading'])) {
          $display->content[$key]->configuration['override_title_heading'] = $pane->configuration['override_title_heading'];
        }

        $data = panels_ajax_pane_encrypt_data(serialize($config));
        drupal_add_js(array('panels_ajax_pane' => array($pid => $data)),'setting');
      }
    }
  }
}

function panels_ajax_pane_menu() {
  $items['panels_ajax_pane/render'] = array(
    'page callback' => 'panels_ajax_pane_render',
    'access callback' => TRUE, 
  );
  return $items;
}

function panels_ajax_pane_render() {
  ctools_include('plugins');
  ctools_include('context');
  ctools_include('content');

  $config = unserialize(panels_ajax_pane_decrypt_data(file_get_contents('php://input')));

  if (empty($config)) {
    // Nothing to display
    drupal_json_output(array('markup' => FALSE, 'title' => FALSE));
    return;
  }

  $contexts = array();
  foreach ($config['context'] as $context_id => $context_config) {
    // Work around entity handling bug in ctools
    if ($context_config['name'] == 'entity') {
      $entity_info = entity_get_info($context_config['keyword']);
      if (empty($entity_info)) {
        $context_config['name'] = 'entity:node';
      }
      else {
        $context_config['name'] = 'entity:' . $context_config['keyword'];
      }
    }

    $contexts[$context_id] = ctools_context_get_context_from_context($context_config, 'context', $context_config['argument']);
  }

  // Grab all CSS for later comparison before clearing it
  $previous_css = drupal_add_css();
    
  // Clear the JavaScript & CSS not related to the pane being rendered.
  drupal_static_reset('drupal_add_js');
  drupal_static_reset('drupal_add_css');

  // fake get paramters in case they are used by the render function
  $stored_GET = $_GET;
  $_GET = $config['_GET'];

  // Generate the pane output
  $pane_output = ctools_content_render(
    $config['type'], 
    $config['subtype'], 
    $config['conf'], 
    $config['keywords'], 
    $config['args'], 
    $contexts
  );

  // Put GET paramters back
  $_GET['q'] = $stored_GET;

  // Render the final output
  if (!empty($pane_output->content)) {
    if (is_array($pane_output->content)) {
      $pane_output->content = drupal_render($pane_output->content);
    }
    $output = panels_ajax_pane_render_output($pane_output->content, $config, $previous_css);
  }
  else {
    $output = FALSE;
  }

  $response = array();
  $response['markup'] = $output;
  $response['title'] = !empty($pane_output->title) ? $pane_output->title : FALSE;

  drupal_json_output($response);
  exit;
}

function panels_ajax_pane_render_output($pane_output, $config, $previous_css = array()) {
  // Grab new CSS files related to this pane
  $css = drupal_add_css();

  // Assign a unique basename to new stylesheets
  foreach ($css as $key => $item) {
    if ($item['type'] == 'file') {
      // If not defined, force a unique basename for this file.
      $basename = isset($item['basename']) ? $item['basename'] : drupal_basename($item['data']);
      $item['basename'] = $basename;
      $css[$key] = $item;
    }
  }
  
  // Iterate through previous CSS stylesheets, and add any with matching basenames to allow
  // for proper overrides when calling drupal_get_css($css)
  foreach ($previous_css as $key => $item) {
    if ($item['type'] == 'file') {
      // If not defined, force a unique basename for this file.
      $basename = isset($item['basename']) ? $item['basename'] : drupal_basename($item['data']);
      $item['basename'] = $basename;
      $previous_css[$key] = $item;
      foreach ($css as $key2 => $item2) {
        // Make sure to append previous stylesheets with matching basenames to the new CSS array
        if ($item['basename'] == $item2['basename']) {
          $css[$key] = $previous_css[$key];    
        }  
      }      
    }
  }  
   
  $output  = '';
  $output .= '<?xml version="1.0" encoding="UTF-8" ?>';
  $output .= '
    <div class="panels-ajax-pane-html"
    version="HTML+RDFa+MathML 1.1"
    xmlns:content="http://purl.org/rss/1.0/modules/content/"
    xmlns:dc="http://purl.org/dc/terms/"
    xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:og="http://ogp.me/ns#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:sioc="http://rdfs.org/sioc/ns#"
    xmlns:sioct="http://rdfs.org/sioc/types#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
    xmlns:mml="http://www.w3.org/1998/Math/MathML">
  ';
  $output .= '<div class="panels-ajax-pane-head">';

  //Need to call page alter so that modules like google analytics can add js
  //We only invoke the page alter if url_enabled is true
  if($vars['url_enabled']) {
    $page = array();
    drupal_alter('page', $page);
  }

  // Get the javascript and filter out the default misc/drupal.js - it's already been added by the calling page
  $javascript = drupal_add_js();
  unset($javascript['misc/drupal.js']);

  // Add the javascript.
  $output .= drupal_get_js('header', $javascript);

  // Add CSS
  $output .= drupal_get_css($css);
  if (!empty($layout['css'])) {
    $output .= "<link rel='stylesheet' type='text/css' href='" . base_path() . $layout['path'] . '/' . $layout['css'] . "' />";
  }

  $output .= '</div>';

  // Print the output of the panel
  $output .= '<div class="panels-ajax-pane-body">';
  $output .= theme('status_messages');
  $output .= '<div class="panels-ajax-pane-panel panels-ajax-pane-panel-' . str_replace('_', '-', $config['plugin']) . '">';
  $output .= $pane_output;
  $output .= '</div class="panels-ajax-pane-body">';
  $output .= drupal_get_js('footer', $javascript);
  $output .= '</div></div>';

  return $output;
}

/**
 * Implements hook_preprocess_panels_pane().
 */
function panels_ajax_pane_preprocess_panels_pane(&$vars) {
  if ($vars['pane']->type == 'panels_ajax_pane_content') {
    $add_class = !empty($vars['pane']->configuration['add_class']) ? $vars['pane']->configuration['add_class'] : array();
    $vars['classes_array'] = !empty($vars['classes_array']) ? array_merge($vars['classes_array'], $add_class) : $add_class;
  }
}