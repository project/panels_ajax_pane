<?php
/**
 * @file
 * All ajax panes are replaced with this pane, which is responsible for doing the AJAX call from the backend
 * It should not be used on it's own. 
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Panels Ajax Pane Placeholder - Do Not Use'),
  'description' => t('All ajax panes are replaced with this pane, which is responsible for doing the AJAX call from the backend. Do not add this pane from the UI.'),
  'render callback' => 'panels_ajax_pane_content_content_type_render', 
  'category' => t('Panels Ajax Pane'),
);

function panels_ajax_pane_content_content_type_render($subtype, $conf, $panel_args, &$context) {
  drupal_add_js(drupal_get_path('module', 'panels_ajax_pane') .'/panels_ajax_pane.js', 'file');

  $block = new stdClass();
  $block->title = '';
  $pid = $conf['pid'];
  $block->content = "<div class='panels-ajax-pane panels-ajax-pane-$pid' data-pid='$pid'></div>"; 
  return $block;
}
